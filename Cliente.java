

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Cliente implements ICliente{

	private Registry registry;
	private String nome;
	private Scanner scanner;
	
	public Cliente(){
		scanner = new Scanner(System.in);
	}

	@Override
	public void imprime(String msg) throws RemoteException {

		System.out.println(msg);

	}

	@Override
	public Integer getRespostaMenu(String msg) throws RemoteException {

		Integer resposta = null;
		boolean respCorreta = true;

		while(respCorreta){
			try {
				System.out.println(msg);
				resposta = scanner.nextInt();
				respCorreta = false;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Resposta inválida! Digite somente números");
			}
		}
		return resposta;
	}

	@Override
	public String getResposta(String msg) throws RemoteException {
		return JOptionPane.showInputDialog(msg);
	}

	public void registrar(String nome){
		try {
			ICliente stubCliente = (ICliente) UnicastRemoteObject.exportObject(this, 0);
			registry = LocateRegistry.getRegistry();
			registry.bind(nome, stubCliente);
		} catch (Exception e) {
			System.out.println("Erro de conexão: " + e);
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public void terminarExecucao(){
		System.exit(0);
	}

	public Registry getRegistry() {
		return registry;
	}

	public void setRegistry(Registry registry) {
		this.registry = registry;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
