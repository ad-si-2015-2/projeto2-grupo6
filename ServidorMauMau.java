
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.AccessException;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author Lucas
 *
 */

public class ServidorMauMau implements IServidor {
	// Endereco IP do servidor, fixo
	private InetAddress ipServidor;
	// indica a porta usada
	private int porta;
	private ServerSocket servidor;
	// jogadores da partida
	private Jogador[] jogadores;
	// baralho da partida
	private Baralho baralho;
	// campeao da partida
	private Jogador campeao;
	// marcador fim de jogo
	private Boolean partidaFinalizada;
	// numero de jogadores que pararam e esperam por resultado
	private int jogadoresParados;

	private int contMonte = 0;

	private int mauMau = 0;

	private Cartas monte = null;

	// Registro RMI
	private Registry registry;
	// Quantidade de jogadores
	private int qtdJogadores;

	public ServidorMauMau(int qtdJogadores) {

		this.qtdJogadores = qtdJogadores;
		this.baralho = new Baralho(1, true);
		this.partidaFinalizada = false;
		this.jogadores = new Jogador[getQtdJogadores()];
	}

	public ServidorMauMau(Jogador[] jogadores, int porta) throws IOException {
		this.jogadores = jogadores;
		this.porta = porta;
		this.servidor = new ServerSocket(this.getPorta());

		if (jogadores.length < 2) {
			// valida��ao numero de jogadores

			// A CRIAR
		} else {
			this.baralho = new Baralho(1, true);
			this.partidaFinalizada = false;
			iniciar();
		}

	}

	public void registrar(String nome) {
		try {

			registry = LocateRegistry.createRegistry(1099);
			IServidor stubServidor = (IServidor) UnicastRemoteObject.exportObject((Remote) this, 0);

			registry.bind(nome, stubServidor);

			System.err.println("Servidor pronto!");
		} catch (Exception e) {
			System.out.println("Erro de conexão: " + e);
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void conecta(String nome) {

		try {

			ICliente stubCliente = (ICliente) registry.lookup(nome);

			if (jogadores[jogadores.length - 1] == null) {

				Jogador jogador = new Jogador();
				jogador.setStubCliente(stubCliente);
				jogador.setNome(nome);

				for (int i = 0; i < jogadores.length; i++) {
					if (jogadores[i] == null) {
						jogadores[i] = jogador;
						i = jogadores.length;
					}
				}

				enviarMsg(jogador, "Voce esta conectado " + jogador.getNome() + "!");

				if (jogadores[jogadores.length - 1] == null)
					enviarMsg(jogador, "Aguardando conexoes....");

				else {
					// Comunica a todos que o jogo vai comecar!
					enviarMsg("O jogo vai comecar em...");

					for (Integer i = 3; i > 0; i--) {
						enviarMsg(i.toString());
						Thread.sleep(1000);
					}

					this.distribuiCartas();

					fecharConexaoJogadores();
				}
			}

			else {
				stubCliente.imprime(
						"Conexao nao aceita, ja existem jogadores suficientes! Por favor aguarde a proxima rodada");
			}
		} catch (AccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void iniciar() {

		while (true) {

			System.out.println("Aguardando conexoes....");

			try {

				if (jogadores[0] == null) {

					jogadores[0] = new Jogador();

					jogadores[0].setConexao(servidor.accept());
					enviarMsg(jogadores[0], "Conexao aceita, bem vindo ao jogo!Qual o seu nome?");

					enviarMsg(jogadores[0], "Aguardando demais jogadores, aguarde " + jogadores[0].getNome());
				}

				else if (jogadores[1] == null) {

					jogadores[1] = new Jogador();

					jogadores[1].setConexao(servidor.accept());
					enviarMsg(jogadores[1], "Conexao aceita, bem vindo ao jogo!Qual o seu nome?");

					// Comunica a todos que o jogo vai comecar!
					enviarMsg("O jogo vai comecar em...");

					for (Integer i = 3; i > 0; i--) {
						enviarMsg(i.toString());
						Thread.sleep(1000);
					}

					this.distribuiCartas();

					fecharConexaoJogadores();

				}

				else {
					Socket s = servidor.accept();
					enviarMsg(jogadores[1],
							"Conexao nao aceita, ja existem jogadores suficientes! Por favor aguarde a proxima rodada");
					s.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Erro ao tentar conexcao com o Jogador");
			}
		}
	}

	// metodo que distribui cartas para os jogadores e verifica o ganhador
	public void distribuiCartas() {

		// contador de cartas/rodadas
		int k = 0;

		while (!partidaFinalizada && k <= 51) {

			for (int i = 0; i < this.jogadores.length; i++) {

				// a primeira rodada, apenas distribui cinco cartas a cada
				// jogador
				if (k < jogadores.length) {
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());

					k++;

				}

				// Se todos decidiram parar para receber o resultado no final do
				// jogo, então o campeao e informado
				else if (this.jogadoresParados == jogadores.length) {

					campeao = new Jogador();
					campeao.setSomaCartas(0);

					for (Jogador j : jogadores) {
						campeao = campeao.getSomaCartas() <= j.getSomaCartas() ? j : campeao;
					}

					this.partidaFinalizada = true;
					i = this.jogadores.length;
					informaCampeao();

				}

				else {
					int count = 0;// a partir da segunda rodada, conferir se
									// algum jogador acabou com todas as cartas
					for (int j = 0; j < jogadores.length; j++) {

						for (Cartas carta : jogadores[j].getMinhasCartas()) {

							if (carta != null) {

								count++;
							}
						}

						if (count == 0) {
							campeao = jogadores[j];
							partidaFinalizada = true;

						}
						j = jogadores.length;
					}
				}

				System.out.println("A PARTIDA FOI FINALIZADA??????? " + partidaFinalizada);
				if ((partidaFinalizada == false) && !jogadores[i].getParouDeJogar()) {

					menu(jogadores[i]);
					enviarMsg(jogadores[i], "\n\nAguarde enquanto os outros jogadores fazem sua jogada");
				}
				// Mostra o vencedor do jogo
				else {
					i = this.jogadores.length;
					informaCampeao();
				}
			}
		}
	}

	/**
	 * Metodo que envia mensagens informando o vencedor do jogo.
	 */
	public void informaCampeao() {

		this.enviarMsg(campeao, "Parabens " + campeao.getNome() + ", voce e o vencedor!\n");
		this.enviarMsg("O jogador(a) " + campeao.getNome() + " ganhou o jogo!!! \n");

	}

	public void fecharConexaoJogadores() {

		enviarMsg("Sua conexao sera fechada!!!");

		for (Jogador j : jogadores) {
			try {
				Thread.sleep(1000);
				j.getConexao().close();
				j = null;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Erro ao fechar conexao");
			}
		}

	}

	public void menu(Jogador jogador) {

		boolean loop = true;

		String opcoes = "Digite uma das opcoes!!\r\n\r\n" + "1-Pegar Carta\r\n"
				+ "2-Desistir, voce deve aguardar ate o jogo terminar ou todos desistirem\r\n" + "3-Descartar\r\n"
				+ "4-Listar minhas cartas\r\n\r\n" + "5 - MAU MAU\r\n\r\n"
				+ "Digite o numero da opcao escolhida: \r\n\r\n";

		while (loop) {

			if (contMonte == 0) {
				monte = baralho.darProximaCarta();
			} else {

			}
			contMonte++;

			enviarMsg(jogador, "A carta do monte e: " + monte);

			// enviarMsg(jogador, opcoes);

			switch (lerMsgMenu(jogador, opcoes)) {
			case 1:
				this.addCartas(jogador);
				loop = false;
				break;
			case 2:
				jogador.setParouDeJogar(true);
				this.jogadoresParados++;
				loop = false;
				break;
			case 3:

				verificaMauMau(jogador, mauMau);
				mauMau = 0;
				enviarMsg(jogador, "Escolha a opcao correspondente a carta que sera descartada: " + "\n\n");
				enviarMsg(jogador, listarCartas(jogador) + "\n\n");
				int op2 = (lerMsgMenu(jogador, opcoes));

				if (jogador.verificaJogada(op2, monte) == true) {
					monte = rmCartas(jogador, op2);
					loop = false;
				} else {
					enviarMsg(jogador, "\nVoce nao pode fazer esta jogada, atente-se para as regras\n");
				}

				break;
			case 4:
				enviarMsg(jogador, listarCartas(jogador) + "\n\n");
				break;
			case 5:
				mauMau = 1;
				verificaMauMau(jogador, mauMau);

				break;

			default:
				enviarMsg(jogador, "Opcao invalida, digite uma correta" + "\n\n");
				break;
			}
		}
	}

	public void addCartas(Jogador jogador) {

		jogador.addCartas(baralho.darProximaCarta());
		enviarMsg(jogador, listarCartas(jogador));

	}

	public Cartas rmCartas(Jogador jogador, int pos) {

		int count = 0;
		Cartas monte = null;
		for (Cartas carta : jogador.getMinhasCartas()) {
			count++;
			if (count == pos) {
				monte = jogador.rmCartas(pos);
			}

		}
		enviarMsg(jogador, listarCartas(jogador));

		return monte;
	}

	public boolean verificaJogada(Jogador jogador, int pos, Cartas monte) {
		boolean resp = false;
		int count = 0;
		for (Cartas carta : jogador.getMinhasCartas()) {
			count++;
			if (count == pos) {
				resp = jogador.verificaJogada(pos, monte);
			}

		}

		return resp;
	}

	public void verificaMauMau(Jogador player, int Mau) {
		// VERIFICA SE JOGADOR PEDIU MAU MAU OU ESQUECEU DE PEDIR
		int count = 0;
		for (Cartas carta : player.getMinhasCartas()) {

			if (carta != null) {
				count++;
			}
		}
		if ((Mau == 0) && (count == 1)) { // Esqueceu de pedir
			enviarMsg("Voce esqueceu de pedir MAU MAU, receba 5 cartas.");
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			Mau = 0;

		} else if ((Mau == 1) && (count != 1)) { // Pediu errado
			enviarMsg("Voce pediu MAU MAU na hora errada!, receba 5 cartas.");
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			Mau = 1;
		} else {
			// Pediu certo ou n�o pediu mau mau na hora certa
		}

	}

	public String listarCartas(Jogador jogador) {

		String mensagem = "\nLista de cartas\n";
		int count = 0;
		for (Cartas carta : jogador.getMinhasCartas()) {
			count++;
			if (carta != null)

				mensagem += String.valueOf(count) + " - " + carta.toString() + "\n";

		}

		return mensagem;

	}

	public Integer lerMsgMenu(Jogador jogador, String msg) {

		try {

			return jogador.getStubCliente().getRespostaMenu(msg);

		} catch (Exception e) {
			System.out.println("Erro na leitura da mensagem");
			return null;
		}
	}

	public Boolean enviarMsg(Jogador jogador, String msg) {

		String msgEnvio = "\n";
		msgEnvio += msg;

		try {

			jogador.getStubCliente().imprime(msgEnvio);

		} catch (IOException e) {
			return false;
		}
		System.out.println(msgEnvio);

		return true;
	}

	/**
	 * Metodo que envia a mensagem para todos os jogadores
	 */
	public Boolean enviarMsg(String msg) {

		String msgEnvio = "\nMensagem para todos os jogadores: ";
		msgEnvio += msg;

		try {

			for (Jogador jogador : jogadores) {

				if (jogador != null)
					jogador.getStubCliente().imprime(msgEnvio);
			}
		} catch (IOException e) {
			return false;
		}
		System.out.println(msgEnvio);

		return true;
	}

	public int getPorta() {
		return porta;
	}

	public void setPorta(int porta) {
		this.porta = porta;
	}

	public ServerSocket getServidor() {
		return servidor;
	}

	public void setServidor(ServerSocket servidor) {
		this.servidor = servidor;
	}

	public int getJogadoresParados() {
		return jogadoresParados;
	}

	public void setJogadoresParados(int jogadoresParados) {
		this.jogadoresParados = jogadoresParados;
	}

	public int getQtdJogadores() {
		return qtdJogadores;
	}

	public void setQtdJogadores(int qtdJogadores) {
		this.qtdJogadores = qtdJogadores;
	}

}